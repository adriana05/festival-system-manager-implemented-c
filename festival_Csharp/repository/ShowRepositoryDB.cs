﻿using festival_Csharp.model;
using festival_Csharp.repository.exceptions;
using festival_Csharp.utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace festival_Csharp.repository
{
    class ShowRepositoryDB : IShowRepository
    {
        private static readonly ILog log = LogManager.GetLogger("ShowRepositoryDB");

        public ShowRepositoryDB()
        {
            log.Info("Initializing Show Database Repository");
        }

        public Show Delete(long id)
        {
            log.InfoFormat("Trying to delete the show with the id: {0}", id);
            IDbConnection connection = DBUtils.getConnection();


            Show show = FindOne(id);
            if(show == null)
            {
                log.InfoFormat("Entity not found with the id: {0}", id);
                throw new RepositoryExceptions("Entity not found");
            }

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "delete from shows where id = @id";

                IDbDataParameter parameterId = cmd.CreateParameter();
                parameterId.ParameterName = "@id";
                parameterId.Value = id;

                cmd.Parameters.Add(parameterId);
                cmd.ExecuteNonQuery();
            }

            return show;
        }
        public IEnumerable<Show> FilterByDate(DateTime dateTime)
        {
            log.InfoFormat("Initializaing FilterByDate with date: {0}", dateTime);
            
            
            IDbConnection connection = DBUtils.getConnection();

            List<Show> shows = new List<Show> { };

            using (var command = connection.CreateCommand())
            {

                command.CommandText =
                    "select id, date(date/1000, 'unixepoch') as date, location, ticketsAvailable , ticketsSold, artist  from shows where DATETIME(DATE(date/1000,'unixepoch')) = @date";

                IDbDataParameter paramDate = command.CreateParameter();
                paramDate.ParameterName = "@date";
                paramDate.Value = dateTime.Date;
                command.Parameters.Add(paramDate);

                using (var resultSet = command.ExecuteReader())
                {

                    while (resultSet.Read())
                    {

                        long id = resultSet.GetInt64(0);
                        DateTime date = resultSet.GetDateTime(1);
                        String location = resultSet.GetString(2);
                        int ticketsAvailable = resultSet.GetInt32(3);
                        int ticketsSold = resultSet.GetInt32(4);
                        String artist = resultSet.GetString(5);

                        Show show = new Show(date,location , ticketsSold, ticketsAvailable, artist);
                        show.Id = id;

                        shows.Add(show);

                    }

                }

                log.InfoFormat("Returning the shows with size: {0}", shows.Count());
                return shows;


            }
        }

        public IEnumerable<Show> FindAll()
        {
            log.Info("Searching for the entire list of shows");
            List<Show> shows = new List<Show> { };

            IDbConnection conn = DBUtils.getConnection();

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT id, location, ticketsSold, ticketsAvailable, datetime(date/1000, 'unixepoch') ,artist FROM shows ";

                using (var resultSet = cmd.ExecuteReader())
                {
                    while (resultSet.Read())
                    {
                        long id = resultSet.GetInt64(0);
                        string location = resultSet.GetString(1);
                        int ticketsSold = resultSet.GetInt32(2);
                        int ticketsAvailable = resultSet.GetInt32(3);
                        DateTime dateTime = resultSet.GetDateTime(4);
                        String artist = resultSet.GetString(5);

                        Show show = new Show(dateTime, location, ticketsSold, ticketsAvailable, artist);
                        show.Id = id;

                        shows.Add(show);
                    }
                }
            }

            log.InfoFormat("Found all shows, size: {0}", shows.Count());
            return shows;

        }

        public Show FindOne(long id)
        {
            log.InfoFormat("Searching for show with id: {0}", id);
            IDbConnection connection = DBUtils.getConnection();


            Show show = null;
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SELECT id, location, ticketsSold, ticketsAvailable, datetime(date/1000, 'unixepoch') ,artist FROM shows WHERE id = @id";

                IDbDataParameter paramId = cmd.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = id;
                cmd.Parameters.Add(paramId);

                
                using (var resultSet = cmd.ExecuteReader())
                {
                    if (resultSet.Read())
                    {
                        string location = resultSet.GetString(1);
                        int ticketsSold = resultSet.GetInt32(2);
                        int ticketsAvailable = resultSet.GetInt32(3);
                        DateTime dateTime = resultSet.GetDateTime(4);
                        String artist = resultSet.GetString(5);

                        show = new Show(dateTime, location, ticketsSold, ticketsAvailable, artist);
                        show.Id = id;
                    }
                }
            }
            return show;
        }

        public Show Save(Show entity)
        {
            log.InfoFormat("Trying to save: {0}", entity);
            IDbConnection conn = DBUtils.getConnection();


            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "insert into shows(location, ticketsSold, ticketsAvailable, date, artist) values(@location, @ticketsSold, @ticketsAvailable, @date, @artist)";

                IDbDataParameter paramLocation = cmd.CreateParameter();
                paramLocation.ParameterName = "@location";
                paramLocation.Value = entity.Location;
                cmd.Parameters.Add(paramLocation);

                IDataParameter paramTicketsSold = cmd.CreateParameter();
                paramTicketsSold.ParameterName = "@ticketsSold";
                paramTicketsSold.Value = entity.NumberTicketsSold;
                cmd.Parameters.Add(paramTicketsSold);

                IDataParameter paramTicketsAvailable = cmd.CreateParameter();
                paramTicketsAvailable.ParameterName = "@ticketsAvailable";
                paramTicketsAvailable.Value = entity.NumberTicketsAvailable;
                cmd.Parameters.Add(paramTicketsAvailable);

                IDataParameter paramDate = cmd.CreateParameter();
                paramDate.ParameterName = "@date";
                paramDate.Value = entity.DateTime;
                cmd.Parameters.Add(paramDate);

                IDataParameter paramArtist = cmd.CreateParameter();
                paramArtist.ParameterName = "@artist";
                paramArtist.Value = entity.Artist;
                cmd.Parameters.Add(paramArtist);

                cmd.ExecuteNonQuery();

            }

            log.InfoFormat("Successfully added: {0}", entity);
            return entity;

        }

        public Show Update(Show entity)
        {
            log.InfoFormat("Updating entity: {0}", entity);

            Show show = FindOne(entity.Id);
            if(show == null)
            {
                log.Error("Entity not found for updating");
                throw new RepositoryExceptions("Entity not found");
            }

            IDbConnection conn = DBUtils.getConnection();

            using(var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "update shows set location = @location, ticketsSold = @ticketsSold, ticketsAvailable = @ticketsAvailable,  artist = @artist where id = @id";
                IDbDataParameter paramLocation = cmd.CreateParameter();
                paramLocation.ParameterName = "@location";
                paramLocation.Value = entity.Location;

                IDbDataParameter paramTicketsSold = cmd.CreateParameter();
                paramTicketsSold.ParameterName = "@ticketsSold";
                paramTicketsSold.Value = entity.NumberTicketsSold;

                IDbDataParameter paramTicketsAvailable = cmd.CreateParameter();
                paramTicketsAvailable.ParameterName = "@ticketsAvailable";
                paramTicketsAvailable.Value = entity.NumberTicketsAvailable;

                IDbDataParameter paramArtist= cmd.CreateParameter();
                paramArtist.ParameterName = "@artist";
                paramArtist.Value = entity.Artist;

                IDbDataParameter paramId = cmd.CreateParameter();
                paramId.ParameterName = "@id";
                paramId.Value = entity.Id;

                cmd.Parameters.Add(paramLocation);
                cmd.Parameters.Add(paramTicketsSold);
                cmd.Parameters.Add(paramTicketsAvailable);
                cmd.Parameters.Add(paramArtist);
                cmd.Parameters.Add(paramId);

                cmd.ExecuteNonQuery();
            }

            log.InfoFormat("Entity {0} updated into {1}", show, entity);
            return show;

        }
    }
}
