﻿using festival_Csharp.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.repository
{
    interface IRepository<ID, E> where E : Entity<ID>
    {
        E FindOne(ID id);
        E Delete(ID id);
        E Save(E entity);
        E Update(E entity);
        IEnumerable<E> FindAll();
    }
}
