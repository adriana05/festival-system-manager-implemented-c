﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.repository.exceptions
{
    class RepositoryExceptions : ApplicationException
    {
        public RepositoryExceptions(string message) : base(message)
        {

        }
    }
}
