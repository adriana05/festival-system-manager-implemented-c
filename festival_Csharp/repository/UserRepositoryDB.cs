﻿using festival_Csharp.model;
using festival_Csharp.utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using festival_Csharp.repository.exceptions;

namespace festival_Csharp.repository
{ 
    class UserRepositoryDB : IUserRepository
    {
        private static readonly ILog log = LogManager.GetLogger("UserRepositoryDB");

        public UserRepositoryDB()
        {
            log.Info("Initializing User Database Repository");
        }

        public User Delete(long id)
        {
            log.InfoFormat("Trying to delete the user with the id: {0}", id);
            IDbConnection connection = DBUtils.getConnection();


            User entity = FindOne(id);
            if (entity == null)
            {
                log.ErrorFormat("Entity not found with this id: {0}", id);
                throw new RepositoryExceptions("Entity not found");
            }


            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "delete from users where id = @id";

                IDbDataParameter parameterId = cmd.CreateParameter();
                parameterId.ParameterName = "@id";
                parameterId.Value = id;

                cmd.Parameters.Add(parameterId);
                cmd.ExecuteNonQuery();
            }

            log.InfoFormat("Succesfully deleted: {0}", entity);
            return entity;
        }

        public User FilterByEmailPassword(string email, string password)
        {
            log.InfoFormat("Find an user by the email {0} and password {1}", email,password);
            IDbConnection connection = DBUtils.getConnection();

            User user = null;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "select * from users where email = @email and password = @password";
                IDbDataParameter emailParam = cmd.CreateParameter();
                emailParam.ParameterName = "@email";
                emailParam.Value = email;
                cmd.Parameters.Add(emailParam);

                IDbDataParameter passwordParam= cmd.CreateParameter();
                passwordParam.ParameterName = "@password";
                passwordParam.Value = password;
                cmd.Parameters.Add(passwordParam);


                using (var resultSet = cmd.ExecuteReader())
                {
                    if (resultSet.Read())
                    {
                        long id = resultSet.GetInt64(0);
                        
                        user = new User(email, password);
                        user.Id = id;
                    }
                }
            }

            log.InfoFormat("User found: {0}", user);
            return user;
        }

        public IEnumerable<User> FindAll()
        {
            log.Info("Searching for the entire list of users");
            List<User> users = new List<User> { };

            IDbConnection conn = DBUtils.getConnection();

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "select * from users";

                using (var resultSet = cmd.ExecuteReader())
                {
                    while (resultSet.Read())
                    {
                        long id = resultSet.GetInt64(0);
                        string email = resultSet.GetString(1);
                        string password = resultSet.GetString(2);

                        User user = new User(email, password);
                        user.Id = id;

                        users.Add(user);
                    }
                }
            }

            log.InfoFormat("Found all users, size: {0}", users.Count());
            return users;

        }

        public User FindOne(long id)
        {
            log.InfoFormat("Find an user by the id: {0}", id);
            IDbConnection connection = DBUtils.getConnection();

            User user = null;

            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "select * from users where id = @id";
                IDbDataParameter idParam = cmd.CreateParameter();
                idParam.ParameterName = "@id";
                idParam.Value = id;
                cmd.Parameters.Add(idParam);

                using (var resultSet = cmd.ExecuteReader())
                {
                    if (resultSet.Read())
                    {
                        String email = resultSet.GetString(1);
                        String password = resultSet.GetString(2);

                        user = new User(email, password);
                    }
                }
            }

            log.InfoFormat("User found: {0}", user);
            return user;
        }

        public User Save(User entity)
        {
            log.InfoFormat("Trying to save: {0}", entity);
            IDbConnection conn = DBUtils.getConnection();


            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "insert into users(email, password) values(@email, @password)";

                IDbDataParameter parameterEmail = cmd.CreateParameter();
                parameterEmail.ParameterName = "@email";
                parameterEmail.Value = entity.Email;
                cmd.Parameters.Add(parameterEmail);

                IDataParameter parameterPassword = cmd.CreateParameter();
                parameterPassword.ParameterName = "@password";
                parameterPassword.Value = entity.Password;
                cmd.Parameters.Add(parameterPassword);

                cmd.ExecuteNonQuery();

            }

            log.InfoFormat("Successfully added: {0}", entity);
            return entity;

        }

        public User Update(User entity)
        {
            log.InfoFormat("Updating user with id: {0}", entity.Id);

            User user = FindOne(entity.Id);
            if (user == null)
            {
                log.ErrorFormat("Entity with id {0} not found", entity.Id);
                throw new RepositoryExceptions("Entity not found");
            }

            IDbConnection conn = DBUtils.getConnection();

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "update users set email = @email, password = @password where id = @id";
                IDbDataParameter paramEmail = cmd.CreateParameter();
                paramEmail.ParameterName = "@email";
                paramEmail.Value = entity.Email;

                IDbDataParameter paramPassword = cmd.CreateParameter();
                paramPassword.ParameterName = "@password";
                paramPassword.Value = entity.Password;

                IDbDataParameter paramID = cmd.CreateParameter();
                paramID.ParameterName = "@id";
                paramID.Value = entity.Id;

                cmd.Parameters.Add(paramEmail);
                cmd.Parameters.Add(paramPassword);
                cmd.Parameters.Add(paramID);

                cmd.ExecuteNonQuery();

            }

            log.InfoFormat("Entity {0} found and updated", user);
            return user;
        }
    }
}
