﻿using festival_Csharp.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.repository
{
    interface IShowRepository : IRepository<long, Show>
    {
        IEnumerable<Show> FilterByDate(DateTime dateTime);
    }
}
