﻿using festival_Csharp.model;
using festival_Csharp.repository.exceptions;
using festival_Csharp.utils;
using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.repository
{
    class TicketRepositoryDB : ITicketRepository
    {
        private static readonly ILog log = LogManager.GetLogger("TicketRepositoryDB");

        public TicketRepositoryDB()
        {
            log.Info("Initializing Ticket Database Repository");
        }

        public Ticket Delete(long id)
        {
            log.InfoFormat("Trying to delete the ticket with the id: {0}", id);
            IDbConnection connection = DBUtils.getConnection();

            
            Ticket entity = FindOne(id);
            if (entity == null)
            {
                log.ErrorFormat("Entity not found with this id: {0}", id);
                throw new RepositoryExceptions("Entity not found");
            }

            
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "delete from tickets where id = @id";

                IDbDataParameter parameterId = cmd.CreateParameter();
                parameterId.ParameterName = "@id";
                parameterId.Value = id;

                cmd.Parameters.Add(parameterId);
                cmd.ExecuteNonQuery();
            }
            
            log.InfoFormat("Succesfully deleted: {0}", entity);
            return null;
        }

        public IEnumerable<Ticket> FindAll()
        {
            log.Info("Searching for the entire list of tickets");
            List<Ticket> tickets = new List<Ticket> { };

            IDbConnection conn = DBUtils.getConnection();

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "select t.id, customerName, seatsBought, idShow, location, ticketsSold, ticketsAvailable, datetime(s.date/1000, 'unixepoch') as date," +
                    " artist FROM tickets t INNER JOIN shows s ON t.idShow = s.id ";
                
                using (var resultSet = cmd.ExecuteReader())
                {
                    while (resultSet.Read())
                    {
                        long id = resultSet.GetInt64(0);
                        string customerName = resultSet.GetString(1);
                        int seatsBought = resultSet.GetInt32(2);
                        long idShow = resultSet.GetInt64(3);
                        string location = resultSet.GetString(4);
                        int ticketsSold = resultSet.GetInt32(5);
                        int ticketsAvailable = resultSet.GetInt32(6);
                        DateTime dateTime = resultSet.GetDateTime(7);
                        string artist = resultSet.GetString(8);

                        Show show = new Show(dateTime, location, ticketsSold, ticketsAvailable, artist);
                        show.Id = idShow;

                        Ticket ticket = new Ticket(seatsBought, customerName, show);
                        ticket.Id = id;

                        tickets.Add(ticket);
                    }
                }
            }

            log.InfoFormat("Found all users, size: {0}", tickets.Count());
            return tickets;

        }

        public Ticket FindOne(long id)
        {
             log.InfoFormat("Find a ticket by the id: {0}", id);
             IDbConnection conn = DBUtils.getConnection();

             Ticket ticket = null;

             using (var cmd = conn.CreateCommand())
             {
                 cmd.CommandText = "select customerName, seatsBought, idShow, location, ticketsSold, ticketsAvailable, datetime(s.date/1000, 'unixepoch') as date," +
                    " artist FROM tickets t INNER JOIN shows s ON t.idShow = s.id WHERE t.id = @id"; 
                 IDbDataParameter idParam = cmd.CreateParameter();
                 idParam.ParameterName = "@id";
                 idParam.Value = id;
                 cmd.Parameters.Add(idParam);

                 using (var resultSet = cmd.ExecuteReader())
                 {
                    if (resultSet.Read())
                    {

                        string customerName = resultSet.GetString(0);
                        int seatsBought = resultSet.GetInt32(1);
                        long idShow = resultSet.GetInt64(2);
                        string location = resultSet.GetString(3);
                        int ticketsSold = resultSet.GetInt32(4);
                        int ticketsAvailable = resultSet.GetInt32(5);
                        DateTime dateTime = resultSet.GetDateTime(6);
                        string artist = resultSet.GetString(7);

                        Show show = new Show(dateTime, location, ticketsSold, ticketsAvailable, artist);
                        show.Id = idShow;

                        ticket = new Ticket(seatsBought, customerName, show);
                        ticket.Id = id;
                    }
                 }
             }
             log.InfoFormat("Ticket found: {0}", ticket);
             return ticket;
            
           
        }

        public Ticket Save(Ticket entity)
        {
            log.InfoFormat("Trying to save: {0}", entity);
            IDbConnection conn = DBUtils.getConnection();


            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "insert into tickets(customerName, seatsBought, idShow) values(@customerName, @seatsBought, @idShow)";

                IDbDataParameter paramCustomerName = cmd.CreateParameter();
                paramCustomerName.ParameterName = "@customerName";
                paramCustomerName.Value = entity.CustomerName;
                cmd.Parameters.Add(paramCustomerName);

                IDataParameter paramSeatsBought = cmd.CreateParameter();
                paramSeatsBought.ParameterName = "@seatsBought";
                paramSeatsBought.Value = entity.NumberTicketsBought;
                cmd.Parameters.Add(paramSeatsBought);

                IDataParameter paramIdShow = cmd.CreateParameter();
                paramIdShow.ParameterName = "@idShow";
                paramIdShow.Value = entity.Show.Id;
                cmd.Parameters.Add(paramIdShow);

                cmd.ExecuteNonQuery();

            }

            log.InfoFormat("Successfully added: {0}", entity);
            return entity;

        }

        public Ticket Update(Ticket entity)
        {
            log.InfoFormat("Updating ticket with id: {0}", entity.Id);

            Ticket ticket= FindOne(entity.Id);
            if (ticket == null)
            {
                log.ErrorFormat("Entity with id {0} not found", entity.Id);
                throw new RepositoryExceptions("Entity not found");
            }

            IDbConnection conn = DBUtils.getConnection();

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = "update tickets set customerName = @customerName, seatsBought = @seatsBought, idShow = @idShow where id = @id";
                IDbDataParameter parameterCustomerName= cmd.CreateParameter();
                parameterCustomerName.ParameterName = "@customerName";
                parameterCustomerName.Value = entity.CustomerName;

                IDbDataParameter paramSeatsBought= cmd.CreateParameter();
                paramSeatsBought.ParameterName = "@seatsBought";
                paramSeatsBought.Value = entity.NumberTicketsBought;


                IDbDataParameter paramIDShow = cmd.CreateParameter();
                paramIDShow.ParameterName = "@idShow";
                paramIDShow.Value = entity.Show.Id;

                IDbDataParameter paramID = cmd.CreateParameter();
                paramID.ParameterName = "@id";
                paramID.Value = entity.Id;

                cmd.Parameters.Add(parameterCustomerName);
                cmd.Parameters.Add(paramSeatsBought);
                cmd.Parameters.Add(paramIDShow);
                cmd.Parameters.Add(paramID);

                cmd.ExecuteNonQuery();

            }

            log.InfoFormat("Entity {0} found and updated", ticket);
            return ticket;
        }
    }
}
