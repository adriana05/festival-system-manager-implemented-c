﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model.validator
{
    class ValidatorException : ApplicationException
    {
        public ValidatorException(String message) : base(message)
        {
        }
    }
}
