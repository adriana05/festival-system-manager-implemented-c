﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model.validator
{
    class TicketValidator : IValidator<Ticket>
    {
        public void Validate(Ticket entity)
        {
            string error = "";

            if(entity.CustomerName == "")
            {
                error += "Customer's name cannot be null";
            }

            if(entity.NumberTicketsBought <= 0)
            {
                error += "Seats bought cannot be less or equal to 0";
            }

            if(error != "")
            {
                throw new ValidatorException(error);
            }
        }
    }
}
