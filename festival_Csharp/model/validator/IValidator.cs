﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model.validator
{
    interface IValidator<E>
    {
        void Validate(E entity);
    }
}
