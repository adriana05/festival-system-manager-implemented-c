﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model.validator
{
    class ShowValidator : IValidator<Show>
    {
        public void Validate(Show entity)
        {
            string error = "";

            if(entity.Location == "")
            {
                error += "Location invalid";
            }

            if (entity.Artist == "")
            {
                error += "Artist invalid";
            }

            if(entity.NumberTicketsSold < 0)
            {
                error += "Tickets sold cannot be less than 0";
            }

            if(entity.NumberTicketsAvailable < 0)
            {
                error += "Tickets available cannot be less than 0";
            }

            if(error != "")
            {
                throw new ValidatorException(error);
            }
        }
    }
}
