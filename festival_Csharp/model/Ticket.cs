﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model
{
    class Ticket : Entity<long>
    {
        public int NumberTicketsBought { get; set; }
        public string CustomerName { get; set; }
        public Show Show { get; set; }

        public Ticket(int numberTicketsBought, string customerName, Show show)
        {
            NumberTicketsBought = numberTicketsBought;
            CustomerName = customerName;
            Show = show;
        }

        public override string ToString()
        {
            return "Ticket: " + Id + " Customer name: " + CustomerName + " Seats bought: " + NumberTicketsBought.ToString() + " " + Show.ToString();
        }

        public override bool Equals(object obj)
        {
            return obj is Ticket ticket &&
                   NumberTicketsBought == ticket.NumberTicketsBought &&
                   CustomerName == ticket.CustomerName;
        }

        public override int GetHashCode()
        {
            int hashCode = -1149274272;
            hashCode = hashCode * -1521134295 + NumberTicketsBought.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(CustomerName);
            return hashCode;
        }
    }
}
