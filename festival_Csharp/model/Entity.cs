﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model
{
    class Entity<ID>
    {
        public ID Id { get; set; }
    }
}
