﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.model
{
    class Show : Entity<long>
    {
        public string Artist { get; set; }
        public string Location { get; set; }
        public DateTime DateTime { get; set; }
        public int NumberTicketsSold { get; set; }
        public int NumberTicketsAvailable { get; set; }
        

        public Show(DateTime dateTime, string location, int numberTicketsSold, int numberTicketsAvailable, string artist)
        {
            DateTime = dateTime;
            Location = location;
            NumberTicketsSold = numberTicketsSold;
            NumberTicketsAvailable = numberTicketsAvailable;
            Artist = artist;
  
        }

        public override bool Equals(object obj)
        {
            return obj is Show spectacle &&
                   Id == spectacle.Id &&
                   DateTime == spectacle.DateTime &&
                   Location == spectacle.Location &&
                   NumberTicketsSold == spectacle.NumberTicketsSold &&
                   NumberTicketsAvailable == spectacle.NumberTicketsAvailable;
        }

        public override string ToString()
        {
            return "Show: " + Id + " Location: " + Location + " Date and time: " + DateTime.ToString() + " Tickets sold: " + NumberTicketsSold.ToString() + " Tickets available: "
                   + NumberTicketsAvailable + " Artist: " + Artist;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
