﻿using festival_Csharp.model;
using festival_Csharp.model.validator;
using festival_Csharp.repository;
using festival_Csharp.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace festival_Csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            UserRepositoryDB userRepositoryDB = new UserRepositoryDB();
            TicketRepositoryDB ticketRepositoryDB = new TicketRepositoryDB();
            ShowRepositoryDB showRepositoryDB = new ShowRepositoryDB();


            User user = new User("petrusi.coman@gmail.com","PetrusiComan");
            user.Id = 6;

            Show show = new Show(DateTime.Now, "Craiova", 45, 90, "Tudor Gheorghe");
            show.Id = 5;

            Ticket ticket = new Ticket(10, "Bologa Cristine", show);
            ticket.Id = 8;

          
            Console.WriteLine(userRepositoryDB.FindOne(6).ToString());
            Console.WriteLine(userRepositoryDB.FindOne(2).ToString());
            foreach(var u in userRepositoryDB.FindAll())
            {
                Console.WriteLine(u.ToString());
            }
            foreach (var s in showRepositoryDB.FindAll())
            {
                Console.WriteLine(s.ToString());
            }
            foreach (var s in ticketRepositoryDB.FindAll())
            {
                Console.WriteLine(s.ToString());
            }
            Console.WriteLine(ticketRepositoryDB.FindOne(1));

            ShowValidator showValidator = new ShowValidator();
            TicketValidator ticketValidator = new TicketValidator();
            UserValidator userValidator = new UserValidator();

            Service service = new Service(showRepositoryDB, ticketRepositoryDB, userRepositoryDB, showValidator, ticketValidator, userValidator);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var login = new LogIn(service);
            Application.Run(login);
            //Application.Run(new Main(service));

        }
    }
}
