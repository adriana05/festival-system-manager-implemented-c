﻿using festival_Csharp.service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace festival_Csharp
{
    partial class Main : Form
    {
        Service Service;

        public  Main(Service service)
        {
            Service = service;
            
            InitializeComponent();
        }


        private void logOutBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            PopulateTableShow();
            SetupDataGridViews();

            artistTxtBox.ReadOnly = true;
            locationTxtBox.ReadOnly = true;
            dateTimeTxtBox.ReadOnly = true;
        }
        private void SetupDataGridViews()
        {

            /* tableShows.ColumnCount = 5;
             tableShows.Columns[0].Name = "Artist";
             tableShows.Columns[1].Name = "Location";
             tableShows.Columns[2].Name = "Date and time";
             tableShows.Columns[3].Name = "Available";
             tableShows.Columns[4].Name = "Sold";*/

            
            tableShows.SelectionMode =
                DataGridViewSelectionMode.FullRowSelect;
            tableShows.MultiSelect = false;
            tableShows.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;


            
            tableShowsFilteredByDate.SelectionMode =
                DataGridViewSelectionMode.FullRowSelect;
            tableShowsFilteredByDate.MultiSelect = false;
            tableShowsFilteredByDate.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }


        private void PopulateTableShow()
        {
            tableShows.DataSource = Service.FindAllShows();

            foreach (DataGridViewRow row in tableShows.Rows)
                if (Convert.ToInt32(row.Cells[4].Value) == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.Red;
                }

            tableShows.Columns["Id"].Visible = false;

        }

        private void PopulateTableShowFilteredByDate()
        {

            DateTime date = dateTimePickerShows.Value;
            tableShowsFilteredByDate.DataSource = Service.FindShowsOnSpecificDate(date);

            foreach (DataGridViewRow row in tableShowsFilteredByDate.Rows)
                if (Convert.ToInt32(row.Cells[4].Value) == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.Red;
                }

            tableShowsFilteredByDate.Columns["Id"].Visible = false;
        }

        private void tableShowsFilteredByDate_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex < 0)
                return;

            var data = tableShows.Rows[e.RowIndex];
            artistTxtBox.Text = data.Cells["Artist"].Value.ToString();
            locationTxtBox.Text = data.Cells["Location"].Value.ToString();
            dateTimeTxtBox.Text = data.Cells["DateTime"].Value.ToString();

            if (Int32.Parse(data.Cells["NumberTicketsAvailable"].Value.ToString()) == 0)
            {
                sellTicketBtn.Enabled = false;
            }
            else
            {
                sellTicketBtn.Enabled = true;
            }

        }

        private void tableShows_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            var data = tableShows.Rows[e.RowIndex];
            artistTxtBox.Text = data.Cells["Artist"].Value.ToString();
            locationTxtBox.Text = data.Cells["Location"].Value.ToString();
            dateTimeTxtBox.Text = data.Cells["DateTime"].Value.ToString();

            if(Int32.Parse(data.Cells["NumberTicketsAvailable"].Value.ToString()) == 0)
            {
                sellTicketBtn.Enabled = false;
            }
            else
            {
                sellTicketBtn.Enabled = true;
            }


        }

        private void sellTicketBtn_Click(object sender, EventArgs e)
        {
            try
            { 
                string name = nameTxtBox.Text;
                int seats = Int32.Parse(seatsTxtBox.Text);
                int idShow = Int32.Parse(tableShows.SelectedRows[0].Cells[5].Value.ToString());
                

                Service.SaveTicket(name, seats, idShow);
                if (tableShows.Visible == true)
                    PopulateTableShow();
                else
                    PopulateTableShowFilteredByDate();

                artistTxtBox.Text = "";
                locationTxtBox.Text = "";
                nameTxtBox.Text = "";
                dateTimeTxtBox.Text = "";
                seatsTxtBox.Text = "";

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void displayAllBtn_Click(object sender, EventArgs e)
        {
            tableShowsFilteredByDate.Visible = false;
            tableShows.Visible = true;

            artistTxtBox.Text = "";
            locationTxtBox.Text = "";
            nameTxtBox.Text = "";
            dateTimeTxtBox.Text = "";
            seatsTxtBox.Text = "";
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            tableShows.Visible = false;
            tableShowsFilteredByDate.Visible = true;
            PopulateTableShowFilteredByDate();

            artistTxtBox.Text = "";
            locationTxtBox.Text = "";
            nameTxtBox.Text = "";
            dateTimeTxtBox.Text = "";
            seatsTxtBox.Text = "";
        }
    }
}
