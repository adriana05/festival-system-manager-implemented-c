﻿using festival_Csharp.model;
using festival_Csharp.model.validator;
using festival_Csharp.repository;
using festival_Csharp.service.exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace festival_Csharp.service
{
    class Service
    {
        IShowRepository ShowRepository;
        ITicketRepository TicketRepository;
        IUserRepository UserRepository;
        ShowValidator ShowValidator;
        TicketValidator TicketValidator;
        UserValidator UserValidator;

        public Service(IShowRepository showRepository, ITicketRepository ticketRepository, IUserRepository userRepository, ShowValidator showValidator, TicketValidator ticketValidator, UserValidator userValidator)
        {
            ShowRepository = showRepository;
            TicketRepository = ticketRepository;
            UserRepository = userRepository;
            ShowValidator = showValidator;
            TicketValidator = ticketValidator;
            UserValidator = userValidator;
        }

        public IEnumerable<Show> FindAllShows()
        {
            return ShowRepository.FindAll();
        }

        public IEnumerable<Show> FindShowsOnSpecificDate(DateTime date)
        {

            return ShowRepository.FilterByDate(date);
        }

        private void UpdateSeats(Show show, int dif)
        {

            show.NumberTicketsSold = show.NumberTicketsSold + dif;
            show.NumberTicketsAvailable = show.NumberTicketsAvailable - dif;

            ShowRepository.Update(show);

        }

        public Ticket SaveTicket(string name, int seats, int idShow)
        {
            Show show = ShowRepository.FindOne(idShow);
            Ticket ticket = new Ticket(seats, name, show);

            if (show.NumberTicketsAvailable < seats)
                throw new ServiceException("Not enough available seats!");

            TicketValidator.Validate(ticket);
            Ticket savedTicket = TicketRepository.Save(ticket);
            UpdateSeats(ticket.Show, ticket.NumberTicketsBought);

            return savedTicket;

        }
        public User Login(String email, String password)
        {

            return UserRepository.FilterByEmailPassword(email, password);

        }
    }
}
